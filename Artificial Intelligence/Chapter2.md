# Chapter 2 - Problems, Problem Spaces, and Search

- To build a system to solve particular problem, we need to do four things.
  1. Define The problem precisely.
     - This definition must include precise specifications of what the initial situations will be as well as what final situations constitute acceptable solutions to the problem.
  2. Analyze the problem.
     - A few very important features can have an immense impact on the appropriateness of vartious possible techniques for solving the problem.
  3. Isolate and represent the task knowledge that is necessary to solve the problem.
  4. Choose the best problem solving technique(s) and apply it(them) to the particular problem.



## Defining the problem as a state space search

- Importance of State Space Representation:
  - It allows for a formal definition of a problem as the need to convert some given situation into some desired situation uisng a set of permissible operations.
  - It permits us to define the process of solving a particular problem as a combination of known techniques (each represented as a rule defining a single step in the space) and search, the general technique of exploring the space to try to find some path from the current state to goal state.  Search is very important process in the solution of hard problems for which no more direct techniques are available.



## Production Systems

